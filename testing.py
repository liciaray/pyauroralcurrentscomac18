#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 24 22:23:37 2017

@author: liciaray
"""
import matplotlib.pyplot as plt
import numpy as np
from Planet import Planet
#from Magnetosphere import Magnetosphere
#from get_nc_flux import get_nc_flux as get_flux

planet = Planet.jupiter()

dr = 1e4
r_inner = 5.0
r_outer = 100.0
n_steps = (r_outer - r_inner)*planet.RP/dr
print(n_steps)

r = np.arange(n_steps)*dr+r_inner*planet.RP
r_edge = np.arange(n_steps+1)*dr + r_inner*planet.RP - dr*0.5


#runtime = timeit.timeit(get_flux(r,planet.RP))
#print(runtime)
flux = planet.fluxNC(r)
s = planet.getS(flux)
theta = planet.getTheta(s)
bm = planet.getBmCANKK(r)
bi = planet.getBi(theta)

#planet.msphere = Magnetosphere.jupiter(r)
plt.semilogy(r/planet.RP,flux)
plt.yscale('log')
plt.title('Test')
plt.xlabel('Radius (R$_{J}$)')
plt.ylabel('Flux Function')