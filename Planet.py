#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Class to describe planets and provide basic properties. Most are constants.
Created on Fri Nov 24 22:39:56 2017

@author: liciaray
"""
from scipy.special import gammaincc, gamma
import numpy as np

class Planet:
    def __init__(self,RP,MP,OP,BP,sp0):
        self._RP = RP
        self._MP = MP
        self._OP = OP
        self._BP = BP
        self._sp0 = sp0
        self.msphere = None
        
#assign radius, mass, angular frequency
#and planetary magnetic field strength
       
    #Jupiter 
    @classmethod
    def jupiter(cls):
        return cls(7.1492e7,1.89813e27,1.7735e-4,4.264e-4,0.1)
    
    #Saturn
    @classmethod
    def saturn(cls):
        return cls(1,5,7,80)
    
    #Jupiter flux function
    def fluxNC(self,rad):
        #returns the equatorial flux function defined in nichols and cowley 2004
        b0     = 3.335e5             #nT
        roe0   = 14.501*self.RP
        a      = 5.4e4               #nT
        m      = 2.71
        r_jup3 = self.RP*self.RP*self.RP   #m^3
        f_inf  = 2.841e4             #flux at infinity in nT*Rj^2
        aa = -2./5.    
        rexp = 5./2.

        #uses gamma function identities to calculate upper gamma function
        upper_gamma = (1./aa)*(gamma(aa+1.)*gammaincc(aa+1,(rad/roe0)**rexp) - 
                       (((rad/roe0)**rexp)**aa)*np.exp(-(rad/roe0)**rexp))

        return (f_inf*self.RP*self.RP + b0*r_jup3/(2.5*roe0)* \
                upper_gamma + a/(m-2.)*(self.RP/rad)**(m-2.)*self.RP*self.RP)/ \
                (self.RP*self.RP)
    
    #s distance
    def getS(self,flux):
        return self.RP*np.sqrt(flux/(self.BP*1e9))
    
    def getTheta(self,s):
        return np.arcsin(s/self.RP)
    
    def getBmCANKK(self,rad):
        b0     = 3.335e5             #nT
        roe0   = 14.501*self.RP
        a      = 5.4e4               #nT
        m      = 2.71
        
        return (b0*(self.RP/rad)**3*np.exp(-(rad/roe0)**2.5) + a*(self.RP/rad)**m)*1e-9
    
    def getBi(self,theta):
        return self.BP*np.sqrt(1.0 + 3.0*(np.cos(theta))**2)
    
#    def __init__(self, ff, s, theta, bm, bi, rm):
#        self._s = s
#        self._theta = theta
#        self._bm = bm
#        self._bi = bi
#        self._rm = rm
#        self._ff = ff
#    
#    
    #Properties and checks    
    @property
    def sp0 (self):
        return self._sp0
    
    @sp0.setter
    def sp0 (self, sp0):
        if sp0 < 0:
            raise ValueError ('Conductance must be positive')
        else:
            self._sp0 = sp0
            
    @property
    def RP (self):
        return self._RP
  
    @property
    def MP (self):
        return self._MP
    
    @property
    def OP (self):
        return self._OP
    
    @property
    def BP (self):
        return self._BP
